package com.example.trainingjava_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingJavaBeApplication {
    public static void main(String[] args) {
        SpringApplication.run(TrainingJavaBeApplication.class, args);
    }

}