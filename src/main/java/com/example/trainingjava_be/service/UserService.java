package com.example.trainingjava_be.service;

import com.example.trainingjava_be.model.User;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
public class UserService {
    private List<User> userList;
    public UserService() {
        userList = new ArrayList<>();

        User user1 = new User(1, "Linh", "baoolink@gmail.com", "admin", "https://i.pinimg.com/564x/47/09/80/470980b112a44064cd88290ac0edf6a6.jpg");
        User user2 = new User(1, "Hoa", "hoa@gmail.com", "admin", "https://i.pinimg.com/564x/12/0c/d0/120cd0fed3be054ef753a624621889f3.jpg");
        User user3 = new User(1, "Vien", "vien@gmail.com", "user", "https://i.pinimg.com/564x/04/a1/9e/04a19ee67f2b334b992bf228db4af21b.jpg");

        userList.addAll(Arrays.asList(user1, user2, user3));

    }

    public List<User> getAllUsers() {
        return userList;
    }
}